# [Equilybrium](https://gitlab.com/DRSCUI/equilybrium-dev)

Equilybrium is a solution that monitors your files and their backup in a flexible way for a specific use case.

For more information, read [``equilybrium-whitepaper``](https://gitlab.com/DRSCUI/equilybrium-dev/-/blob/main/equilybrium-whitepaper.md).
